'use strict';


/**
 * uploads an source file.
 *
 *
 * file File file to upload
 * returns ApiResponse
 **/

var formatData = require('./datas');
var actions = require('./actions');
var path = require('path');

exports.downloadFile = function (file) {
    console.log('info', '-------------------------- Start download --------------------------');
    if (path.extname(file) === '.txt') {
        console.log('info', '-- Check if file exist --------------------------');
        var filePath = path.resolve(__dirname, file);
        if (fs.existsSync(filePath)) {
            console.log('info', '-- Read file --------------------------');
            const data = fs.readFileSync(filePath, 'utf8')
            return new Promise(function (resolve, reject) {
                console.log('info', '-------------------------- Send download --------------------------');
                resolve(data);
            });
        }
        console.log('error', '-- file doesn\'t exist --------------------------');
        return new Promise(function (resolve, reject) {
            resolve({
                error: "File dont exist"
            });
        });
    } else {
        console.log('error', '-- Wrong file extension asked --------------------------');
        return new Promise(function (resolve, reject) {
            resolve({
                error: "Only txt allowed"
            });
        });
    }
};

exports.uploadFile = function (file) {
    console.log('info', '-------------------------- New file --------------------------');
    if (file.mimetype === 'text/plain') {
        console.log('info', 'Parse data Start --------------------------');
        var data = formatData.createDatas(file.buffer);
        console.log('info', 'Parse data End --------------------------');

        console.log('info', 'Set starting position --------------------------');
        var position = {
            x: data.adventurer.xcoord,
            y: data.adventurer.ycoord,
            orientation: data.adventurer.orientation,
        }

        console.log('info', 'Start loop over all actions detected --------------------------');

        for (var action = 0; action < data.adventurer.actions.length; action++) {
            // Choose between actions
            switch (data.adventurer.actions[action]) {
                case 'a':
                    console.log('info', 'This turn we move --------------------------');
                    position = actions.doit('move', '', position, data);
                    break;
                case 'g':
                    console.log('info', 'This turn we change direction to the left --------------------------');
                    position = actions.doit('turn', 'left', position, data);
                    break;
                case 'd':
                    console.log('info', 'This turn we change direction to the right --------------------------');
                    position = actions.doit('turn', 'right', position, data);
                    break;
                default:
                    console.log('error', 'Unknown action --------------------------');
            }
        }

        console.log('info', 'All actions has be done --------------------------');

        data.adventurer.xcoord = position.x;
        data.adventurer.ycoord = position.y;
        data.adventurer.orientation = position.orientation;
        data.treasuresLeft = data.treasures.length;
        delete data.adventurer.actions;
        console.log('info', 'Write resulting data --------------------------');
        formatData.writeDatas(data);
        delete data.treasures;
        console.log('info', 'Writing is over --------------------------');


        console.log('info', 'Send data to front --------------------------');
        console.log('info', data);
        return new Promise(function (resolve, reject) {
                resolve(data);
        });
    } else {
        console.log('error', '-------------------------- Wrong file format --------------------------');
        return new Promise(function (resolve, reject) {
            resolve({
                error: "Wrong file type"
            });
        });
    }
};
