function doit(action, direction, position, data) {
    console.log('info', '-- Start action --------------------------');
    switch (position.orientation) {
        //determine which orientation do action
        case 's':
            if (action === "turn") {
                console.log('info', '-- TURN ------------------------');
                if (direction === "left") {
                    console.log('info', '---- LEFT ----------------------');
                    position.orientation = 'e';
                } else if (direction === "right") {
                    console.log('info', '---- RIGHT ----------------------');
                    position.orientation = 'o';
                }
            } else if (action === "move") {
                console.log('info', '-- MOVE SOUTH Y+1 ----------------------');

                if (checkLimit(position, 's', data)) {
                    position.y = parseInt(position.y) + 1;
                    checkChests(position, data);
                }
            }
            break;
        case 'n':
            if (action === "turn") {
                console.log('info', '-- TURN ------------------------');

                if (direction === "left") {
                    console.log('info', '---- LEFT ----------------------');

                    position.orientation = 'o';
                } else if (direction === "right") {
                    console.log('info', '---- RIGHT ----------------------');

                    position.orientation = 'e';
                }
            } else if (action === "move") {
                console.log('info', '-- MOVE NORTH Y-1 ----------------------');
                if (checkLimit(position, 'n', data)) {
                    position.y = parseInt(position.y) - 1;
                    checkChests(position, data);
                }
            }
            break;
        case 'e':
            if (action === "turn") {
                console.log('info', '-- TURN ------------------------');
                if (direction === "left") {
                    console.log('info', '---- LEFT ----------------------');
                    position.orientation = 'n';
                } else if (direction === "right") {
                    console.log('info', '---- RIGHT ----------------------');
                    position.orientation = 's';
                }
            } else if (action === "move") {
                console.log('info', '-- MOVE EST X+1 ----------------------');
                if (checkLimit(position, 'e', data)) {
                    position.x = position.x + 1;
                    checkChests(position, data);
                }
            }
            break;
        case 'o':
            if (action === "turn") {
                console.log('info', '-- TURN ------------------------');
                if (direction === "left") {
                    console.log('info', '---- LEFT ----------------------');
                    position.orientation = 's';
                } else if (direction === "right") {
                    console.log('info', '---- RIGHT ----------------------');
                    position.orientation = 'n';
                }
            } else if (action === "move") {
                console.log('info', '-- MOVE OUEST X-1 ----------------------');
                if (checkLimit(position, 'o', data)) {
                    position.x = position.x - 1;
                    checkChests(position, data);
                }
            }
            break;
        default:
    }
    console.log('info', 'End action --------------------------');
    return position;
}

function checkMountains(newPosition, mountains) {
    console.log('info', '-- Start mountain(s) check --------------------------');
    if (mountains.length > 0) {
        for (var mountain = 0; mountain < mountains.length; mountain++) {
            if (parseInt(mountains[mountain].xcoord) === parseInt(newPosition.x) && parseInt(mountains[mountain].ycoord) === parseInt(newPosition.y)) {
                console.log('info', '--- Block by mountain --------------------------');
                return false;
            }
        }
        console.log('info', '--- No mountain in sight --------------------------');
        return true;
    }
}

function checkChests(position, data) {
    console.log('info', '-- Start chest(s) check --------------------------');
    var treasureFound = false;
    var nBtreasure = data.treasuresFound;

    if (data.treasures.length > 0) {
        //Loop over treasures left and test if where are in the spot
        for (var treasure = 0; treasure < data.treasures.length; treasure++) {
            if (parseInt(data.treasures[treasure].xcoord) === parseInt(position.x) && parseInt(data.treasures[treasure].ycoord) === parseInt(position.y)) {
                console.log('info', '--- Found a chest \\o/ --------------------------');
                treasureFound = treasure;
                nBtreasure++;
                break;
            }
        }
        // log treasure and remove one
        if (treasureFound !== false) {
            data.treasures.splice(treasureFound, 1);
            data.treasuresFound = nBtreasure;
        } else {
            console.log('info', '--- No chest this turn /o\\ --------------------------');
        }
    }
}

function checkLimit(position, direction, data) {
    console.log('info', '-- Start limits check --------------------------');
    var newPosition = {
        x: position.x,
        y: position.y,
    };
    var realMap = {
        width: data.map.width,
        height: data.map.height,
    };
    realMap.width = realMap.width - 1;
    realMap.height = realMap.height - 1;
    if ((realMap.width < position.x) || (realMap.height < position.y) || (position.y < 0) || (position.x < 0)) {
        console.log('error', '--- Outside of the map , are you lost ? --------------------------');
        return false;
    } else {
        // check our direction and test if we move , can we go.
        switch (direction) {
            case 's':
                newPosition.y = parseInt(position.y) + 1;
                if (realMap.height >= newPosition.y) {
                    if (checkMountains(newPosition, data.mountains)) {
                        return true;
                    } else {
                        return false;
                    }
                }
                break;
            case 'n':
                newPosition.y = parseInt(position.y) - 1;
                if (newPosition.y >= 0) {
                    if (checkMountains(newPosition, data.mountains)) {
                        return true;
                    } else {
                        return false;
                    }
                }
                break;
            case 'e':
                newPosition.x = parseInt(position.x) + 1;
                if (realMap.width >= newPosition.x) {
                    if (checkMountains(newPosition, data.mountains)) {
                        return true;
                    } else {
                        return false;
                    }
                }
                break;
            case 'o':
                newPosition.x = parseInt(position.x) - 1;
                if (newPosition.x >= 0) {
                    if (checkMountains(newPosition, data.mountains)) {
                        return true;
                    } else {
                        return false;
                    }
                }
                break;
            default:
                return false;
        }
        console.log('error', '--- Can \'t go further, turn around ! --------------------------');
        return false;
    }
}

module.exports.doit = doit;
