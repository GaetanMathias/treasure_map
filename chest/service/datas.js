fs = require('fs');
path = require('path');

function createDatas(fileBuffer) {
    console.log('info', '-- Start construct of data object --------------------------');
    var data = {
        adventurer: {
            name: "",
            xcoord: 0,
            ycoord: 0,
            orientation: "",
            actions: "",
        },
        downloadLink: "/downloadFile/result.txt:",
        map: {
            width: 0,
            height: 0,
        },
        mountains: [],
        treasures: [],
        treasuresFound: 0,
    };
    console.log('info', '-- Split buffer into array => each line of buffer = one array line --------------------------');
    var payload = fileBuffer.toString().split(/(?:\r\n|\r|\n)/g);
    if (payload && payload.length) {
        // Loop over array
        for (var line = 0; line < payload.length; line++) {
            // Split each lines into array of element and trim it
            payload[line] = payload[line].split('-').map(function (item) {
                return item.trim();
            });
            // Test minimum on line element length
            if (payload[line].length >= 3) {
                // Find which kind of line
                switch (payload[line][0].toLowerCase()) {
                    case 'c':
                        data.map = {
                            width: payload[line][1],
                            height: payload[line][2]
                        };
                        break;
                    case 'm':
                        data.mountains.push({
                            xcoord: payload[line][1],
                            ycoord: payload[line][2]
                        });
                        break;
                    case 't':
                        if (payload[line].length === 4) {
                            if (payload[line][3] > 0) {
                                i = 0;
                                while (i < payload[line][3]) {
                                    data.treasures.push({
                                        xcoord: payload[line][1],
                                        ycoord: payload[line][2]
                                    });
                                    i++;
                                }
                            }
                        }
                        break;
                    case 'a':
                        if (payload[line].length === 6) {
                            data.adventurer = {
                                name: payload[line][1],
                                xcoord: payload[line][2],
                                ycoord: payload[line][3],
                                orientation: payload[line][4].toLowerCase(),
                                actions: payload[line][5].split('').map(function (item) {
                                    return item.toLowerCase();
                                }),
                            };
                        }
                        break;
                    default:
                }
            }
        }
    }
    console.log('info', '-- Data object ready --------------------------');
    return data;
}

function writeDatas(data) {
    console.log('info', '-- Start Writing --------------------------');
    console.log('info', '--- Check if file exist --------------------------');
    if (fs.existsSync(path.resolve(__dirname, 'result.txt'))) {
        //file exists
        console.log('info', '--- File exist, erase it --------------------------');
        fs.unlinkSync(path.resolve(__dirname, 'result.txt'));
        console.log('info', '--- File deleted --------------------------');
    } else {
        console.log('info', '--- File don\'t exist go on --------------------------');
    }
    console.log('info', '-- Init file --------------------------');
    var logger = fs.createWriteStream(path.resolve(__dirname, 'result.txt'), {
        flags: 'a' // 'a' means appending (old data will be preserved)
    });
    console.log('info', '-- Write map line --------------------------');
    if (typeof (data.map) !== "undefined") {
        logger.write(`C - ${data.map.width} - ${data.map.height}\r\n`)
    }
    console.log('info', '-- Write mountain(s) line(s) --------------------------');
    if (typeof (data.mountains) !== "undefined") {
        if (data.mountains.length > 0) {
            for (var mountain = 0; mountain < data.mountains.length; mountain++) {
                logger.write(`M - ${data.mountains[mountain].xcoord} - ${data.mountains[mountain].ycoord}\r\n`);
            }
        }
    }
    console.log('info', '-- Write treasure(s) line(s) --------------------------');
    if (typeof (data.treasures) !== "undefined") {
        if (data.treasures.length > 0) {
            var nbTreasure = 0;
            // Loop over all treasures
            for (var treasure = 0; treasure < data.treasures.length; treasure++) {
                // check if first line
                if (treasure !== 0) {
                    // Not first line so test if current line === previous line
                    // If equals increment number
                    // If NOT reset count and write previous line
                    if (data.treasures[treasure].xcoord === data.treasures[treasure - 1].xcoord && data.treasures[treasure].ycoord === data.treasures[treasure - 1].ycoord) {
                        nbTreasure++;
                        // IF last line write it
                        if (treasure === (data.treasures.length - 1)) {
                            logger.write(`T - ${data.treasures[treasure].xcoord} - ${data.treasures[treasure].ycoord} - ${nbTreasure}\r\n`);
                        }
                    } else {
                        logger.write(`T - ${data.treasures[treasure - 1].xcoord} - ${data.treasures[treasure - 1].ycoord} - ${nbTreasure}\r\n`);
                        nbTreasure = 1;
                    }
                } else {
                    nbTreasure++;
                }
            }
        }
    }
    console.log('info', '-- Write adventurer line --------------------------');
    if (typeof (data.adventurer) !== "undefined") {
        logger.write(`A - ${data.adventurer.name} - ${data.adventurer.xcoord} - ${data.adventurer.ycoord} - ${data.adventurer.orientation.toUpperCase()} - ${data.treasuresFound}\r\n`);
    }
    logger.end();
    console.log('info', '-- End writing, close it --------------------------');
}

module.exports.createDatas = createDatas;
module.exports.writeDatas = writeDatas;
