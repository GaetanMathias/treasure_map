'use strict';

var utils = require('../utils/writer.js');
var Script = require('../service/ScriptService');

module.exports.downloadFile = function downloadFile(req, res, next) {
    var file = req.swagger.params['fileName'].value;
    Script.downloadFile(file)
        .then(function (response) {
            res.setHeader("Content-Disposition", "attachment; filename=" + file);
            res.setHeader("Content-Type", "text/plain; charset=utf-8");
            res.write(response);
            res.end();
        })
        .catch(function (response) {
            utils.writeJson(res, response);
        });
};

module.exports.uploadFile = function uploadFile(req, res, next) {
    var file = req.swagger.params['file'].value;
    Script.uploadFile(file)
        .then(function (response) {
            utils.writeJson(res, response);
        })
        .catch(function (response) {
            utils.writeJson(res, response);
        });
};
