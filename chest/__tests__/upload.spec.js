const request = require('supertest');
const fs = require('mz/fs');
const app = request('../index');
const req = request('http://localhost:8090');
let testFilePath = null;
describe('POST /uploadFile - upload a new file', () => {
    const filePath = `${__dirname}/test.txt`;

    // Upload test file
    it('should upload the test file to server', () =>
        // Test if the test file is exist
        fs.exists(filePath)
        .then((exists) => {
            if (!exists) throw new Error('file does not exist');
            return req(app)
                .post('/uploadFile')
                .attach('file', filePath)
                .then((res) => {
                    console.log("res :", res);
                    // const {
                    //     success,
                    //     message,
                    //     filePath
                    // } = res.body;
                    // expect(success).toBeTruthy();
                    // expect(message).toBe('Uploaded successfully');
                    // expect(typeof filePath).toBeTruthy();
                    // // store file data for following tests
                    // testFilePath = filePath;
                })
                .catch(err => console.log(err));
        })
    );

});
