import {
    mount
} from '@vue/test-utils';
import boardmap from '../src/components/boardmap.vue';

describe('Boardmap', () => {
    test('is a Vue instance', () => {
        const wrapper = mount(boardmap);
        expect(wrapper.isVueInstance()).toBeTruthy();
    });
});
