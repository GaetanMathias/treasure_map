/**
 * Get files to load
 */
exports.getFiles = () => ({
    stylesheets: [
        '../src/assets/base.less',
    ],
});
