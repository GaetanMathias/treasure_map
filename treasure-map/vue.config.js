/* jshint esversion: 9 */

// Additional configuration for Vue
const webpack = require('webpack');
const softPackage = require('./package.json');
const lessConfig = require('./config/less.js');

module.exports = {
    // baseUrl: `http://${host}:${port}/v2/`,
    productionSourceMap: false,
    configureWebpack: {
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    PACKAGE_JSON: `"${escape(JSON.stringify(softPackage))}"`,
                },
            }),
        ],
    },
    chainWebpack: (config) => {
        config
            .plugin('provide')
            .use(webpack.ProvidePlugin, [{
                $: 'jquery',
                jquery: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                'window.$': 'jquery',
                Popper: ['popper.js', 'default'],
                Util: 'exports-loader?Util!bootstrap/js/dist/util',
                Dropdown: 'exports-loader?Dropdown!bootstrap/js/dist/dropdown',
            }]);
    },
    pages: {
        index: {
            // entry for the page
            entry: 'src/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            filename: 'index.html',
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Treasure Map',
        },
    },

    css: {
        loaderOptions: {
            less: {
                // http://lesscss.org/usage/#less-options-strict-units `Global Variables`
                // `primary` is global variables fields name
                javascriptEnabled: true,
            },
        },
    },
    pluginOptions: {
        'style-resources-loader': {
            preProcessor: 'less',
            patterns: lessConfig.getFiles().stylesheets,
        },
    },
};
